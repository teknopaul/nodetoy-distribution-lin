#!/bin/bash

BACK=`pwd`
cd `dirname $0`
BASE=`pwd`

version=`node print-ver.js`
echo building nodetoy version $version

if [ -d ./prepare ] ; then
	echo deleting old version
	rm -rf ./prepare
fi

mkdir prepare
#echo copy in nodetoy
cp --archive ../nodetoy prepare

#echo del git and eclipse settings
rm -rf prepare/nodetoy/.git 
rm -rf prepare/nodetoy/.settings

#echo copy in dependencies
mkdir prepare/nodetoy/node_modules
cd ../node_modules
cp --archive dom-js filter-chain nodeunit rewrite sax $BASE/prepare/nodetoy/node_modules
cd $BASE

#echo TODO copy portable node

#echo build tar
cd prepare
mv nodetoy nodetoy-$version
tar cfz ../nodetoy-$version.tar.gz nodetoy-$version
mv nodetoy-$version nodetoy

cd $BASE

cp nodetoy-$version.tar.gz /var/www/publish/binaries/nodetoy

ls -la /var/www/publish/binaries/nodetoy/nodetoy-$version.tar.gz

cd $BACK

